﻿using System.ComponentModel;

namespace Cadastro.Domain.Entities
{
    public class Product: BaseModel
    {
        [DisplayName("Nome")]
        public string Name { get; set; }
        [DisplayName("Preço")]
        public decimal Value { get; set; }
        [DisplayName("Ativo")]
        public bool Active { get; set; }
        [DisplayName("Categoria")]
        public int IdCategory { get; set; }
        [DisplayName("Categoria")]
        public virtual Category Category { get; set; }
    }
}